.syntax unified

.global _Reset
_Reset:
  LDR sp, =stack_top
  BL entry
  B .

.section .text
entry:

  MOV r3, #0 @ index

  MOV r4, #-1 @ First number
  MOV r5, #-1 @ Second number

  MOV r6, #0 @ Accumulator
loop:
  @ Read input
  LDR r2, =.data
  LDRB r2, [r2, r3]

  @ Stop if it's 10
  @ CMP r3, #6
  @ BEQ stop

  @ Stop if it's null
  CMP r2, #0
  BEQ stop

  @ Do state machine shit
  MOV r1, #1
state_machine_loop:
  @ Load index
  LDRB r9, =indices
  ADD r9, r1
  LDRB r10, [r9]

  @ r1 and r10 as input
  @ returns the string character in r7
  BL load_string_char

  @ Check if the current char progresses the state machine
  CMP r7, r2
  BEQ state_machine_progress
  @ Invalid next character, reset machine
  MOV r12, #0
  STRB r12, [r9]
  B state_machine_continue
state_machine_progress:
  @ Increment by 1
  ADD r10, #1
  @ Check if this finishes the word
  BL load_string_char
  CMP r7, #0
  BNE state_machine_not_done 
  
  @ It's done!
  @ Save number
  PUSH {r2}
  MOV r2, r1
  BL found_number
  POP {r2}

  PUSH {r0, r1, r2}
  LDR r0, =bleh
  BL print_str
  POP {r0, r1, r2}

  MOV r10, #0
state_machine_not_done:
  @ It's not done
  STRB r10, [r9]
state_machine_continue:
  @ Debug print
  MOV r0, r1
  PUSH {r1}
  BL print_digit
  MOV r0, r7
  PUSH {r1}
  BL print_char
  PUSH {r0, r1}
  BL print_newline

  @ Run for 1..=9
  ADD r1, #1
  CMP r1, #10
BNE state_machine_loop

  @ Check if it's a digit
  CMP r2, #0x30
  BLT not_digit
  CMP r2, #0x39
  BGT not_digit
  @ It's a digit
  ADD r2, #-0x30 @ ASCII to u32

  BL found_number

not_digit:
  @ Is it a newline?
  CMP r2, #0x0a
  BNE continue
  @ Yes


  MOV r0, #10
  MUL r4, r0
  ADD r4, r5
  ADD r6, r4

  @ Reset first and second
  MOV r4, #-1 @ First number
  MOV r5, #-1 @ Second number

  @ Print about it
  PUSH {r0, r1, r2, lr}
  LDR r0, =new_line
  BL print_str
  POP {r0, r1, r2, lr}


continue:
  ADD r3, #1
  B loop

stop:
  B stop
@ Dumb stuff

@ Argument is r0
@ Save r1
print_digit:
  ADD r0, 0x30
print_char:
  LDR r1, =0x101f1000 @ output
  STRB r0, [r1]

  POP {r1}
  BX lr

@ Save r0, r1
print_newline:
  MOV r0, 0x0a
  LDR r1, =0x101f1000 @ output
  STRB r0, [r1]

  POP {r0, r1}
  BX lr

@ Argument is r0
@ Save r1, r2
print_str:
  LDR r1, =0x101f1000 @ output
print_str_loop:
  LDRB r2, [r0]
  CMP r2, #0
  BEQ print_str_exit
  STRB r2, [r1]
  ADD r0, #1
  B print_str_loop
print_str_exit:
  BX lr

@ Argument r1, r10
@ FIXME: this is bad
load_string_char:
  @ Load string address
  @ r7 = *(one+6*(r1-1the current char progresses the state machine) + indices[r9])
  MOV r7, r1
  ADD r7, #-1
  MOV r8, #6
  MUL r7, r8
  LDR r8, =one
  ADD r7, r8
  ADD r7, r10
  LDRB r7, [r7] @ next expected char
  
  BX lr

@ Argument r2
@ Globals r4, r5
found_number:
  @ Debug
  PUSH {r0, r1, r2, lr}
  LDR r0, =YO
  BL print_str
  POP {r0, r1, r2, lr}

  @ Check if it's the first time
  CMP r4, #-1
  BNE second_time
  @ It's the first time
  MOV r4, r2
  MOV r5, r2
  B found_number_exit
second_time:
  MOV r5, r2

found_number_exit:
  PUSH {lr}
  @ Debug print
  PUSH {r0}
  MOV r0, r4
  PUSH {r1}
  BL print_digit
  MOV r0, r5
  PUSH {r1}
  BL print_digit
  PUSH {r0, r1}
  BL print_newline
  POP {r0}
  POP {lr}

  BX lr

.section .data
.incbin "input"
.byte 0

one:
.string "one\0\0"
.string "two\0\0"
.string "three"
.string "four\0"
.string "five\0"
.string "six\0\0"
.string "seven"
.string "eight"
.string "nine\0"

indices:
.fill 9, 1, 0

YO:
.string "YO NEW NUMBER\n"
bleh:
.string "bleh\n"
new_line:
.string "New line!\n"