.syntax unified

.global _Reset
_Reset:
  LDR sp, =stack_top
  BL entry
  B .

.section .text
entry:

  MOV r3, #0 @ index

  MOV r4, #-1 @ First number
  MOV r5, #-1 @ Second number

  MOV r6, #0 @ Accumulator
loop:
  @ Read input
  LDR r2, =.data
  LDRB r2, [r2, r3]

  @ Check if it's null
  CMP r2, #0
  BEQ stop

  @ Check if it's a digit
  CMP r2, #0x30
  BLT not_digit
  CMP r2, #0x39
  BGT not_digit

  @ It's a digit
  ADD r2, #-0x30 @ ASCII to u32

  @ Check if it's the first time
  CMP r4, #-1
  BNE second_time
  @ It's the first time
  MOV r4, r2
  MOV r5, r2
  B continue

second_time:
  MOV r5, r2
  B continue

not_digit:
  @ Is it a newline?
  CMP r2, #0x0a
  BNE continue
  @ Yes
  @ Debug print
  @ MOV r0, r4
  @ PUSH {r1}
  @ BL print_digit
  @ MOV r0, r5
  @ PUSH {r1}
  @ BL print_digit
  @ PUSH {r0, r1}
  @ BL print_newline

  MOV r0, #10
  MUL r4, r0
  ADD r4, r5
  ADD r6, r4

  @ Reset first and second
  MOV r4, #-1 @ First number
  MOV r5, #-1 @ Second number

continue:
  ADD r3, #1
  B loop

stop:
  B stop
@ Dumb stuff


@ Argument is r0
@ Save r1
print_digit:
  ADD r0, 0x30
  LDR r1, =0x101f1000 @ output
  STRB r0, [r1]

  POP {r1}
  BX lr

@ Save r0, r1
print_newline:
  MOV r0, 0x0a
  LDR r1, =0x101f1000 @ output
  STRB r0, [r1]

  POP {r0, r1}
  BX lr

.section .data
.incbin "input"
.byte 0
